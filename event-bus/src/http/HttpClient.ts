import axios from 'axios';
import { config } from '../config';

export const usersServiceClient
    = axios.create({ baseURL: config.users_service.url });

export const messagesServiceClient
    = axios.create({ baseURL: config.messages_service.url });