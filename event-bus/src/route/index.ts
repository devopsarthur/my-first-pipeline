import Router from 'express';
import { events } from './events';

const router = Router();


router.use('/app', events);

export { router };