import { Router } from 'express';
import { EventsController } from '../controller/EventsController';
import { validateToken } from '@virta-msg/virta-common';

const router = Router();

router.post('/events', validateToken, EventsController.handleReceivedEvent);

/**
 * An endpoint for other microservices to fetch all events from event-bus.
 * A use case of this endpoint is that if one service is down then up again, it could replay and re-handle all events to catch up the state of the system.
 */
router.get('/events', validateToken, EventsController.fetchEvents);

export { router as events };