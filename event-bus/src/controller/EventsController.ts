import {Request, Response} from 'express';
import { default as ah } from 'express-async-handler';
import { EventData } from '@virta-msg/virta-common';
import { publish } from '../Emitter';

export class EventsController {
    // cache all events as a MVP solution, it can also be persisted in database.
    static eventsStore: EventData[] = [];

    static handleReceivedEvent = ah(async (req: Request, res: Response): Promise<void> => {
        const jwtPayload = res.locals.jwtPayload;
        const { id, username } = jwtPayload;
        const authToken = req.headers.authorization as string;
        const event = req.body as EventData;
        console.log(`EVENT BUS got event: ${JSON.stringify(event)}`);

        // cache events (can be persisted to db)
        EventsController.eventsStore.push(event);

        // publish event to other microservices
        await publish(event, authToken);
        res.status(200).send({});
    });

    
    static fetchEvents = ah(async (req: Request, res: Response): Promise<void> => {
        //TODO: issue in response
        res.status(200).send({ events: EventsController.eventsStore });
    });
}