
export const config = {
    /**
     * All host names are the names of the "Cluster IP" services of k8s cluster for
     * inter-cluster communication. 
     * On terminal, run `kubectl get services` to check that out.
     */
    users_service: {
        url: 'http://users-srv:3003'
    },

    messages_service: {
        url: 'http://messages-srv:3002'
    }
}
