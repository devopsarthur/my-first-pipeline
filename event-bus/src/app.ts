import express from 'express';
import { router } from './route';
import { errorHandler } from '@virta-msg/virta-common';

const app = express();
app.use(express.json());

// to let our express app trusts traffic from our ingress-nginx proxy
app.set('trust proxy', true);

// routes
app.use('/', router);

app.use(errorHandler);
export { app };