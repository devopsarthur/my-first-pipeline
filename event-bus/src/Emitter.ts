import { EventData } from '@virta-msg/virta-common';
import { usersServiceClient, messagesServiceClient } from './http/HttpClient';

export const publish = async (event: EventData, authToken: string) => {
  setAuthToken(authToken);
  
  try {
    await usersServiceClient.post('/app/events', event);
  } catch (err) {
    console.error(`Failed to publish event to users-service! ${JSON.stringify(err)}`);
  }

  try {
    await messagesServiceClient.post('/app/events', event);
  } catch (err) {
    console.error(`Failed to publish event to messages-service! ${JSON.stringify(err)}`);
  }
};

const setAuthToken = (authToken: string) => {
  usersServiceClient.defaults.headers.common['Authorization'] = authToken;
  messagesServiceClient.defaults.headers.common['Authorization'] = authToken;
};