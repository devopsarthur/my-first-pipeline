export const config = {
    database: {
        uri: process.env.MONGO_URI || null
    },

    jwt: {
        authTokenDuration: '1h',
        secret: process.env.JWT_SECRET as string || 'abcd'
    },

    eventbus: {
        url: 'http://event-bus-srv:3001'
    },

    server: {
        port: 3003
    }
}
