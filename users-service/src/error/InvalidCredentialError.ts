import { ErrorBase } from "@virta-msg/virta-common";

export class InvalidCredentialError extends ErrorBase {
    statusCode = 400;
    msg = "Invalid credential!";

    constructor() {
        super('');
        Object.setPrototypeOf(this, InvalidCredentialError.prototype);
    }
    errorInfo() {
        return [{message: this.msg}];
    }

}