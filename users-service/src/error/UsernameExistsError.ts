import { ErrorBase } from "@virta-msg/virta-common";

export class UsernameExistsError extends ErrorBase {
    statusCode = 402;
    msg = "Username exists!";

    constructor() {
        super('');
        Object.setPrototypeOf(this, UsernameExistsError.prototype);
    }
    errorInfo() {
        return [{message: this.msg}];
    }

}