import { ErrorBase } from "@virta-msg/virta-common";

export class UserNotFoundError extends ErrorBase {
    statusCode = 404;
    msg = "User not found!";

    constructor() {
        super('');
        Object.setPrototypeOf(this, UserNotFoundError.prototype);
    }
    errorInfo() {
        return [{message: this.msg}];
    }

}