import mongoose from 'mongoose';

/**
 * @SenderModel: Represents the senders collection, through which we run queries on senders collection
 * 
 * @SenderDoc: Represents a single sender
 */


// we need to teach typescript the different properties the constructor of Sender expects
interface ISenderAttrs {
    username: string;
    date: Date;
}

// the schema for creating SenderModel
const senderSchema = new mongoose.Schema<ISenderAttrs>({
    username: {
        type: String, 
        required: true,
    },

    date: {
        type: Date, 
        required: true,
    }
}, {
    toJSON: {
        transform(doc, ret) {
            // rename the '_id' property from mongoose to 'id'
            ret.id = ret._id;
            delete ret._id;

            // delete '__v' property that is created by mongoose
            delete ret.__v;
        }
    }
});

// SenderDoc has more properties than what we provided in Sender's constructor. We need to teach typescript the properties we hope existing on the Sender
export interface SenderDoc extends mongoose.Document {
    username: string,
    date: Date
}

// a interface to teach typescript that we want to have a builder method in SenderModel
interface ISenderModel extends mongoose.Model<SenderDoc> {
    build(attrs: ISenderAttrs): SenderDoc
}

//let's have the builder method to create a new sender. We only allow the expected attributes/properties via SenderAttrs interface when creating a new sender
senderSchema.statics.build = (attrs: ISenderAttrs) => {
    return new SenderModel(attrs);
}

const SenderModel = mongoose.model<SenderDoc, ISenderModel>('Sender', senderSchema);

export { SenderModel as Sender };