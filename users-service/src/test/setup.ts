import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

// before all tests begin to run, spin up a database instance in memory
let mongo: any;
beforeAll(async () => {
    mongo = await MongoMemoryServer.create();
    const mongoUri = await mongo.getUri();
    await mongoose.connect(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
});

// after all tests are run, stop the database instance
afterAll(async () => {
    await mongo.stop();
    await mongoose.connection.close();
});

beforeEach(async () => {
    // delete data in db so that always have a clean start for each test case
    const collections = await mongoose.connection.db.collections();
    collections.forEach(async cl => {
        await cl.deleteMany({});
    });
});