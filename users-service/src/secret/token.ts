import jwt from 'jsonwebtoken';
import { config } from '../config';
import { UserDoc } from '../model/User';

export const createAuthToken = async (user: UserDoc) : Promise<string> => {
    const payload = {
        id: user.id,
        username: user.username,
    }

    const options = {expiresIn: config.jwt.authTokenDuration}

    const asyncSign = new Promise<string>((resolve, reject) => {
        // get jwt secret from environment variable of k8s pod, 'Secret' object is declared in '/infra/k8s/users-service-deployment.yaml'
        const jwtSecret = config.jwt.secret;

        jwt.sign(payload, jwtSecret, options, (err, token) => {
            if (err) {
                reject(err);
            } else {
                token ? resolve(token) : reject(new Error('Error generating jwt: undefined'));
            }
        });
    });

    return asyncSign;
}