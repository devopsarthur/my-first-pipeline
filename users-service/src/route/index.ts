import { Router } from 'express';
import { users } from './users';
import { events } from './events';

const router = Router();

router.use('/app', users);
router.use('/app', events);
export { router };