import { Router } from 'express';
import { UsersController } from '../controller/UsersController';
import { body } from 'express-validator';
import { validateRequest, validateToken } from '@virta-msg/virta-common';

const router = Router();

const signUpRule = [
    body('username')
        .notEmpty()
        .withMessage('Username is missing.'),
    body('password')
        .trim()
        .isLength({ min: 4 })
        .withMessage('password should at least 4 characters.')
];

const signInRule = [
    body('username')
        .notEmpty()
        .withMessage('Username is missing.'),
    body('password')
        .notEmpty()
        .withMessage('password is missing.')
];

router.post('/users/register', signUpRule, validateRequest, UsersController.registerUser);

router.post('/users/signin', signInRule, validateRequest, UsersController.signIn);


router.get('/users', validateToken, UsersController.getUsers);

router.get('/users/landing', UsersController.landing);

export { router as users };