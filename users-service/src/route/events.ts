import { Router } from 'express';
import { EventsController } from '../controller/EventsController';
import { validateToken } from '@virta-msg/virta-common';
const router = Router();

router.post('/events', [validateToken], EventsController.handleReceivedEvent);

export { router as events };