import request from 'supertest';
import { app } from '../../app';

it('returns 402 error when register with an existing username.', async () => {
    const username = 'foo';
    const password = 'pwd123'; // 1st time registeration is successful

    let response = await request(app).post('/app/users/register').send({
      username,
      password
    });
    expect(response.statusCode).toEqual(201); // 2nd time registeration with the same username should be a failure

    response = await request(app).post('/app/users/register').send({
      username,
      password
    });
    expect(response.statusCode).toEqual(402);
});


it('returns 400 error when register with empty username.', async () => {
    const username = '';
    const password = 'pwd123'; // 1st time registeration is successful

    let response = await request(app).post('/app/users/register').send({
      username
    });
    expect(response.statusCode).toEqual(400);
});


it('returns 400 error when register with empty password.', async () => {
    const username = 'foo';
    const password = ''; // 1st time registeration is successful

    let response = await request(app)
      .post('/app/users/register')
      .send({ username, password});
    expect(response.statusCode).toEqual(400);
});


it('can register a username successfully.', async () => {
    const username = 'foo';
    const password = 'pwd123';
    const response = await request(app).post('/app/users/register').send({
      username,
      password
    });
    expect(response.statusCode).toEqual(201);
});


it('returns authToken when registered successfully.', async () => {
    const username = 'foo';
    const password = 'pwd123';
    const response = await request(app).post('/app/users/register').send({
      username,
      password
    });
    expect(response.body.authToken).toBeDefined();
    expect(response.body.authToken.length).not.toEqual(0);
});