import request from 'supertest';
import { app } from '../../app';

const registerUser = async (username: string, password: string) => {
    await request(app)
        .post('/app/users/register')
        .send({ username, password })
        .expect(201);
}

it('returns 404 error when login with unregistered account.', async () => {
    const username = 'foo';
    const password = 'pwd123';

    let response = await request(app)
        .post('/app/users/signin')
        .send({ username, password });
    expect(response.statusCode).toEqual(404);
});

it('returns \'User not found!\' error message when login with unregistered account.', async () => {
    const username = 'foo';
    const password = 'pwd123';

    let response = await request(app)
        .post('/app/users/signin')
        .send({ username, password });
    
    expect(response.text).toContain('User not found!');
});

it('returns 200 when login with a registered account.', async () => {
    const username = 'foo';
    const password = 'pwd123';
    await registerUser(username, password);

    let response = await request(app)
        .post('/app/users/signin')
        .send({username, password});
    expect(response.statusCode).toEqual(200);
});