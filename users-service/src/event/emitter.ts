import {EventType, EventData, eventBusClient, UserAccount } from '@virta-msg/virta-common';

export const emitUserRegEvent = async (id: number, userAccount: UserAccount, authToken: string) => {

    const evtData: EventData = {
        type: EventType.USER_REG,
        data: {
            id, value: JSON.stringify(userAccount)
        }
    }
    try {
        eventBusClient.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
        await eventBusClient.post('/app/events', evtData);
        
    } catch (err) {
        console.log(`Failed emitting UserRegEvent event to event-bus ${err}`);
    }
    
}

export const emitUserSignInEvent = async (id: number, userAccount: UserAccount, authToken: string) => {

    const evtData: EventData = {
        type: EventType.USER_SIGN_IN,
        data: {
            id, value: JSON.stringify(userAccount)
        }
    }
    try {
        eventBusClient.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
        await eventBusClient.post('/app/events', evtData);
        
    } catch (err) {
        console.log(`Failed emitting UserSignInEvent event to event-bus ${err}`);
    }
    
}