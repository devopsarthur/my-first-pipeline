import {Request, Response} from 'express';
import { default as ah } from 'express-async-handler'; // for handling errors in async handler
import { isEqual, UserAccount } from '@virta-msg/virta-common';
import { UsernameExistsError } from '../error/UsernameExistsError';
import { UserNotFoundError } from '../error/UserNotFoundError';
import { emitUserRegEvent, emitUserSignInEvent } from '../event/emitter';
import { Sender } from '../model/Sender';
import { User } from '../model/User';
import { createAuthToken } from '../secret/token';
import { InvalidCredentialError } from '../error/InvalidCredentialError';

interface UserDTO {
    username: string,
    available: boolean
}

export class UsersController {
    static registerUser = ah(async (req: Request, res: Response): Promise<void> => {
        const { username, password } = req.body
        
        // alternatively we could also:
        // option 1. move this logic to`usernameRule` in users route validator
        // option 2. add flag `unique:true` in `userSchema` of User model. Then, catch error when saving user to database
        const sameUsername = await User.findOne({ username });
        if (sameUsername) {
            throw new UsernameExistsError();
        }

        const user = User.build({ username, password });
        await user.save();

        // generate auth token for user
        const authToken = await createAuthToken(user);

        // send a event to event bus
        const userAccount = { username: user.username, hashedPassword: user.password } as UserAccount;
        
        const env = process.env.NODE_ENV;
        if (process.env.NODE_ENV !== 'test') {
            await emitUserRegEvent(user.id, userAccount, authToken);
        }

        res.status(201).send({ authToken });
    });

    static signIn = ah(async (req: Request, res: Response): Promise<void> => {
        const { username, password } = req.body
        const foundUser = await User.findOne({ username });
        if (!foundUser) {
            throw new UserNotFoundError();
        }

        const isCorrectPwd = await isEqual(foundUser.password, password);
        if (!isCorrectPwd) {
            throw new InvalidCredentialError();
        }

        // generate auth token for user
        const authToken = await createAuthToken(foundUser);

        // send a event to event bus
        const userAccount = { username: foundUser.username, hashedPassword: foundUser.password } as UserAccount;
        
        if (process.env.NODE_ENV !== 'test') {
            await emitUserSignInEvent(foundUser.id, userAccount, authToken);
        }

        res.status(200).send({ authToken });
    });



    static getUsers = ah(async (req: Request, res: Response): Promise<void> => {
        // get available senders
        const tenMinutesAgo = new Date();
        tenMinutesAgo.setTime(tenMinutesAgo.getTime() - 10 * 60 * 1000);
        
        const availableSenders = await Sender.find({ date: { $gt: tenMinutesAgo } })

        const availableSenderNames = availableSenders.map(sender => sender.username);

        console.log(`available sender names: ${availableSenderNames}`);

        // get all users & construct userDTO
        const users = await User.find({});
        const userDtoArr: UserDTO[] = users.map(usr =>
        ({
            username: usr.username,
            available: availableSenderNames.includes(usr.username)
        })
        );

        res.status(200).send({users: userDtoArr });
    });

    // it is only for my testing purpose, not part of the assignment's requirements
    static landing = ah(async (req: Request, res: Response): Promise<void> => {
        res.send('Hi, Virta messenger is landed!');
    });


}