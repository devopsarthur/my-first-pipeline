import { Request, Response } from 'express';
import { default as ah } from 'express-async-handler';
import { EventType, EventData, MessageEventData, DatabaseNotInSyncError, UserAccount } from '@virta-msg/virta-common';
import { Sender } from '../model/Sender';
import { User } from '../model/User';
import { UserNotFoundError } from '../error/UserNotFoundError';

export class EventsController {
    static handleReceivedEvent = ah(async (req: Request, res: Response): Promise<void> => {
        const { id, username } = res.locals.jwtPayload;
        const event = req.body as EventData;
        console.log(`received event: ${JSON.stringify(event)}`);

        await EventsController.handleEvent(event);

        res.status(200).send({});
    });

    public static handleEvent = async (event: EventData) => {
        if (event.type === EventType.USER_REG) {
            await EventsController.handleUserRegEvent(event);
        }
        
        if (event.type === EventType.MSG_CREATE) {
            EventsController.handleMsgCreateEvent(event);
        }

    }

    private static handleUserRegEvent = async (event: EventData) => {
        const userAccount = JSON.parse(event.data.value) as UserAccount;
        const found = await User.find({ username: userAccount.username });
        if (found.length === 0) {
            // TODO: if not found we throw error, hopefully there is another service taking care of database syncing
            throw new DatabaseNotInSyncError();

            // if not found we save to users db
            // const user = User.build({ username });
            // await user.save();
        }
    }

    private static handleMsgCreateEvent = async (event: EventData) => {
        const msgEventData = JSON.parse(event.data.value) as MessageEventData

        const { sender: username, receiver, message, date } = msgEventData;
        console.log(`msg evt sender: ${username}, date: ${date}`);

        // to be sure, let's check if the sender is in our Users collection
        const senderUsers = await User.find({ username });
        if (senderUsers.length === 0) {
            throw new UserNotFoundError();
        }

        // store the sender to db
        const sender = Sender.build({ username, date });
        await sender.save();
    }
}