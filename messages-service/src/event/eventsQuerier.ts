
import { EventData, eventBusClient } from '@virta-msg/virta-common';

/**
 * The purose of this function is to make sure this service doesn't
 * miss any events of the whole system during the time when it was down.
 * 
 * So, it fetches all recorded events from event-bus. 
 * Upper caller is supposed to call EventsController to replay events & handle them.
 * */
export const queryEventHistory = async (serviceToken: string) : Promise<EventData[] | null> => {
    
    try {
        eventBusClient.defaults.headers.common['Authorization'] = `Bearer ${serviceToken}`;
        const response = await eventBusClient.get('/app/events');
        return response.data?.events;
        
    } catch (err) {
        console.log(`Failed emitting UserSignInEvent event to event-bus ${err}`);
    }
    return null;
}

