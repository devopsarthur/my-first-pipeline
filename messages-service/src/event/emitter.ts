import { EventType, EventData, eventBusClient, MessageEventData } from '@virta-msg/virta-common';
import { MessageDoc } from '../model/Message';

export const emitMsgCreationEvent
    = async (message: MessageDoc, authToken: string) => {

        const evtValue: MessageEventData = {
            sender: message.sender,
            receiver: message.receiver,
            message: message.message,
            date: message.date,
            hash: message.hash
        }
        const evtData: EventData = {
            type: EventType.MSG_CREATE,
            data: {
                id: message.id, value: JSON.stringify(evtValue)
            }
        }
        try {
            eventBusClient.defaults.headers.common['Authorization'] = `${authToken}`;
            await eventBusClient.post('/app/events', evtData);
            
        } catch (err) {
            console.log(`Failed emitting emitMsgCreationEvent event to event-bus ${JSON.stringify(err)}`);
        }
    
}