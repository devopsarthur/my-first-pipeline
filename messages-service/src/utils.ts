import mongoose from 'mongoose';
import { config } from './config';
import { DatabaseConnError } from '@virta-msg/virta-common';

export const checkEnvVariable = () => {
    if (!config.database.uri) {
        throw new DatabaseConnError('database uri is missing in environment!');
    }
}

export const connectDB = async () => {
    try {
        // connecting to the MongoDB instance that is in another k8s pod via ClusterIP service
        // See: `/infra/k8s/users-mongo-deployment.yaml`
        await mongoose.connect(config.database.uri!, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log('messages-service server connected to MongoDB.');
    } catch (err) {
        throw new DatabaseConnError();
    }
     
}

