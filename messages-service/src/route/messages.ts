import { Router } from 'express';
import { body, param } from 'express-validator';
import { MessagesController } from '../controller/MessagesController';
import { validateRequest, validateToken } from '@virta-msg/virta-common';
import { validateMessageReceiver } from '../middelware/validateMessageReceiver';

const router = Router();

const messageRule = [
    body('message')
        .notEmpty()
        .withMessage('Can\'t send empty message!'),
    param('username')
        .notEmpty()
        .withMessage('`username` parameter is missing!')
];

router.post('/messages/users/:username',
    messageRule,
    validateRequest,
    validateToken,
    MessagesController.createMessage);

router.get('/messages/users/:username',
    validateToken,
    validateMessageReceiver,
    MessagesController.getMessages);

export { router as messages };