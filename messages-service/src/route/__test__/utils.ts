import { UserMock, createAuthTokenForTest, toHash } from '@virta-msg/virta-common';
import { config } from '../../config';
import { User, UserDoc } from '../../model/User';

export interface UserForTest {
    authToken: string | null,
    username: string
}

export const prepareTestUser = async (username: string, password: string): Promise <UserForTest> => {
    const token = await registerUser(username, password);
    const user = {
        authToken: token,
        username: username
    } as UserForTest;

    return user;
}

export const prepareTestUserUnregistered = async (username: string): Promise <UserForTest> => {
    const user = {
        authToken: null,
        username: username
    } as UserForTest;

    return user;
}

const generateAuthToken = async (userDoc: UserDoc) => {
    const id = userDoc.id;
    const username = userDoc.username;
    const userMock = { id, username } as UserMock;
    return await createAuthTokenForTest(userMock, config.jwt.secret);
}

const registerUser = async (username: string, password: string) => {
    const hashedPwd = await toHash(password);
    const user = await User.build({ username, password: hashedPwd });
    const userDoc = await user.save();
    return await generateAuthToken(userDoc);
}


