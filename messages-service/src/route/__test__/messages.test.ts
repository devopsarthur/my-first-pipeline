import request from 'supertest';
import { app } from '../../app';
import { prepareTestUser, prepareTestUserUnregistered, UserForTest } from './utils';

const sendMessageAsync = async (sender: UserForTest, recipient: UserForTest, message: string) => {
    return await request(app)
        .post(`/app/messages/users/${recipient?.username}`)
        .set('Authorization', `Bearer ${sender?.authToken}`)
        .send(messagePayload(message));
}

const messagePayload = (message: string) => {
    return {message: message}
}

it('can successfully send message to another registered user', async () => {
    const sender = await prepareTestUser('foo', 'password123');
    const recipient = await prepareTestUser('bar', 'password456');
    // user1 send a message to user2
    const response = await sendMessageAsync(
        sender,
        recipient,
        'Hello from user 1');
    
    expect(response.status).toEqual(201);
})


it('returns an \'Receiver not found!\' error if receiver is NOT a registered user', async () => {
    const sender = await prepareTestUser('foo', 'password123');
    const recipient = await prepareTestUserUnregistered('Unregistered');
    // user1 send a message to user2 who is not registered
    const response = await sendMessageAsync(
        sender,
        recipient,
        'Hello from user 1');
    
    expect(response.status).toEqual(404);
    expect(response.text).toContain('Receiver not found!');
})


it('returns \'Can\'t send empty message!\' error if message body is empty', async () => {
    const sender = await prepareTestUser('foo', 'password123');
    const recipient = await prepareTestUser('bar', 'password456');
    const message = '';

    // user1 send a message to user2 who is not registered
    const response = await sendMessageAsync(
        sender,
        recipient,
        message);
    
    expect(response.status).toEqual(400);
    expect(response.text).toContain('Can\'t send empty message!');
})