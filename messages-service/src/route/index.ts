import { Router } from 'express';
import { messages } from './messages';
import { events } from './events';

const router = Router();

router.use('/app', messages);
router.use('/app', events);

export { router };