import { EventData } from '@virta-msg/virta-common';
import { app } from './app';
import { config } from './config';
import { EventsController } from './controller/EventsController';
import { queryEventHistory } from './event/eventsQuerier';
import { checkEnvVariable, connectDB } from './utils';

const startSever = async () => {
    checkEnvVariable();    
    await connectDB();
    
    app.listen(config.server.port, async () => {
        console.log(`messages-service server listening on port ${config.server.port}`);

        /** TODO:
         * when starting, we fetch events from event-bus to make sure this service
         * doesn't miss any events during the time when it was down.
         */
        // try {
        //     const serviceToken = getServiceToken();
        //     const events = await queryEventHistory(serviceToken);
        //     if (events) {
        //         await handleEventHistory(events);
        //     }
        // } catch (err) {
        //     console.log(`While re-playing events: ${JSON.stringify(err)}`);
        // }
   
    })
}

const handleEventHistory = async (events: EventData[]) => {
    events.forEach(async evt => {
        console.log(`Processing event ${evt.type}`)
        await EventsController.handleEvent(evt);
    });
}

startSever();
