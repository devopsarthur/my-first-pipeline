import { Request, Response, NextFunction } from 'express';
import { AccessMessagesForbidden } from '../error/AccessMessagesForbidden';

/**
 * This middleware makes sure that user can only get messages sent to himself/herself.
 */
export const validateMessageReceiver = (
    req: Request,
    res: Response,
    next: NextFunction): void => {
    
    const { username: receiver } = req.params;
    const { id, username: loggedInUser } = res.locals.jwtPayload;

    if (receiver !== loggedInUser) {
        throw new AccessMessagesForbidden();
    }
    next();

}