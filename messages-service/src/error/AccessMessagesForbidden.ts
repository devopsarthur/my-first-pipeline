import { ErrorBase } from "@virta-msg/virta-common";

export class AccessMessagesForbidden extends ErrorBase {
    statusCode = 400;
    msg = "Access messages forbidden!";

    constructor(errorMsg?: string) {
        super(`${errorMsg}`);
        Object.setPrototypeOf(this, AccessMessagesForbidden.prototype);
    }
    errorInfo() {
        return [{ message: `${this.msg}` }];
    }

}