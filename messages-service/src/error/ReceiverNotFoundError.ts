import { ErrorBase } from "@virta-msg/virta-common";

export class ReceiverNotFoundError extends ErrorBase {
    statusCode = 404;
    msg = "Receiver not found!";

    constructor() {
        super('');
        Object.setPrototypeOf(this, ReceiverNotFoundError.prototype);
    }
    errorInfo() {
        return [{message: this.msg}];
    }

}