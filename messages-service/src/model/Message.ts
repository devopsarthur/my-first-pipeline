import mongoose from 'mongoose';

/**
 * @MessageModel: Represents the messages collection, through which we run queries on messages collection
 * 
 * @MessageDoc: Represents a single message
 */


// we need to teach typescript the different properties the constructor of Message expects
interface IMessageAttrs {
    message: string;
    sender: string;
    receiver: string;
    date: Date;
    hash: string;
}

// the schema for creating MessageModel
const messageSchema = new mongoose.Schema<IMessageAttrs>({
    message: {
        type: String, // js type for mongoose
        required: true,
    },
    sender: {
        type: String, 
        required: true,
    },
    receiver: {
        type: String, 
        required: true,
    },
    date: {
        type: Date, 
        required: true,
    },
    hash: {
        type: String, 
        required: true,
    }
}, {
    toJSON: {
        transform(doc, ret) {
            // rename the '_id' property from mongoose to 'id'
            ret.id = ret._id;
            delete ret._id;

            // delete '__v' property that is created by mongoose
            delete ret.__v;
        }
    }
});

// MessageDoc has more properties than what we provided in Message's constructor. We need to teach typescript the properties we hope existing on the Message
export interface MessageDoc extends mongoose.Document {
    message: string;
    sender: string;
    receiver: string;
    date: Date;
    hash: string;
}

// a interface to teach typescript that we want to have a builder method in MessageModel
interface IMessageModel extends mongoose.Model<MessageDoc> {
    build(attrs: IMessageAttrs): MessageDoc
}

//let's have the builder method to create a new message. We only allow the expected attributes/properties via MessageAttrs interface when creating a new message
messageSchema.statics.build = (attrs: IMessageAttrs) => {
    return new MessageModel(attrs);
}

const MessageModel = mongoose.model<MessageDoc, IMessageModel>('Message', messageSchema);

export { MessageModel as Message };