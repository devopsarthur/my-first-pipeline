import mongoose from 'mongoose';
import { toHash } from '@virta-msg/virta-common';

/**
 * @UserModel: Represents the users collection, through which we run queries on users collection
 * 
 * @UserDoc: Represents a single user
 */


// we need to teach typescript the different properties the constructor of User expects
interface IUserAttrs {
    username: string;
    password: string;
}

// the schema for creating UserModel
const userSchema = new mongoose.Schema<IUserAttrs>({
    username: {
        type: String, // js type for mongoose
        required: true,
    },
    password: {
        type: String,
        required: true,
    }
}, {
    toJSON: {
        transform(doc, ret) {
            // rename the '_id' property from mongoose to 'id'
            ret.id = ret._id;
            delete ret._id;

            // delete '__v' property that is created by mongoose
            delete ret.__v;
        }
    }
});

// UserDoc has more properties than what we provided in User's constructor. We need to teach typescript the properties we hope existing on the User
export interface UserDoc extends mongoose.Document {
    username: string;
    password: string;
}

// a interface to teach typescript that we want to have a builder method in UserModel
interface IUserModel extends mongoose.Model<UserDoc> {
    build(attrs: IUserAttrs): UserDoc
}

//let's have the builder method to create a new user. We only allow the expected attributes/properties via UserAttrs interface when creating a new user
userSchema.statics.build = (attrs: IUserAttrs) => {
    return new UserModel(attrs);
}

const UserModel = mongoose.model<UserDoc, IUserModel>('User', userSchema);

export { UserModel as User };