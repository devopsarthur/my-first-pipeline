export const config = {
    database: {
        uri: process.env.MONGO_URI || null
    },
    
    server: {
        port: 3002
    },

    jwt: {
        secret: process.env.JWT_SECRET || 'abcd'
    },
}
