import { Request, Response } from 'express';
import { default as ah } from 'express-async-handler';
import { EventType, EventData, toHash } from '@virta-msg/virta-common';
import { Message } from '../model/Message';
import { User } from '../model/User';
import { emitMsgCreationEvent } from '../event/emitter';
import { ReceiverNotFoundError } from '../error/ReceiverNotFoundError';

interface MessageDTO {
    message: string,
    sender: string,
    date: Date
}

export class MessagesController {
    static createMessage = ah(async (req: Request, res: Response): Promise<void> => {
        const authToken = req.headers.authorization as string;
        const { id: sender_id, username: sender } = res.locals.jwtPayload;
        const { username: receiver } = req.params;
        const msgContent = req.body.message;
        console.log(`sender id: ${sender_id}, sender username: ${sender}, receiver : ${receiver}, msg: ${msgContent}`);
        console.log(`authToken: ${authToken}`);

        // check if receiver is valid, if not throw exception
        const foundReceiver = await User.find({ username: receiver });
        console.log(`found receiver: ${foundReceiver}`);
        if (foundReceiver.length === 0) {
            throw new ReceiverNotFoundError();
        }

        const currentDatetime = new Date();
        const hash = await toHash(currentDatetime.toUTCString());
        const message = Message.build({
            sender,
            receiver,
            message: msgContent,
            date: currentDatetime,
            hash
        });

        await message.save();

        if (process.env.NODE_ENV !== 'test') {
            // send a event to event bus
            await emitMsgCreationEvent(message, authToken);
        }

        res.status(201).send({ message });

    });

    static getMessages = ah(async (req: Request, res: Response): Promise<void> => {
        const { username: receiver } = req.params;
        
        const messages = await Message.find({ receiver }).sort({ date: -1 });
        const messageDtoArr: MessageDTO[] = messages.map(msg => ({
            message: msg.message,
            sender: msg.sender,
            date: msg.date
        }));

        res.status(200).send({ messages: messageDtoArr });
    });

}