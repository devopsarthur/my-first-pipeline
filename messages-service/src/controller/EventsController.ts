import { Request, Response } from 'express';
import { default as ah } from 'express-async-handler';
import { EventType, EventData, MessageEventData, UserAccount } from '@virta-msg/virta-common';
import { Message } from '../model/Message';
import { User } from '../model/User';

export class EventsController {
    static handleReceivedEvent = ah(async (req: Request, res: Response): Promise<void> => {
        const { id, username } = res.locals.jwtPayload;
        const event = req.body as EventData;
        console.log(`received event: ${JSON.stringify(event)}`);

        await EventsController.handleEvent(event);

        res.send({});
    });

    // only handle events that the microservice is intereted
    public static handleEvent = async (event: EventData) => {
        if (event.type === EventType.MSG_CREATE) {
            await EventsController.handleMsgCreateEvent(event);
        }
        if (event.type === EventType.USER_REG) {
            await EventsController.handleUserRegEvent(event);
        }
    }

    private static handleMsgCreateEvent = async (event: EventData) => {
        const { sender, receiver, message, date, hash }
            = JSON.parse(event.data.value) as MessageEventData;

        console.log(`received MSG_CREATE event, sender: ${sender}, receiver: ${receiver}, message: ${message}, date: ${date}, hash: ${hash} `);

        const found = await Message.find({ hash });
        if (found.length === 0) {
            console.log('Does not find message in db, let us create one instead.');
            const msg = Message.build({
                sender,
                receiver,
                message,
                date,
                hash
            });
            await msg.save();
        } else {
            console.log(`msg found in db: ${JSON.stringify(found)}`);
        }
    }

    private static handleUserRegEvent = async (event: EventData) => {
        const userAccount = JSON.parse(event.data.value) as UserAccount;
        const found = await User.find({ username: userAccount.username });
        if (found.length === 0) {
            const user = User.build({
                username: userAccount.username,
                password: userAccount.hashedPassword
            });
            await user.save();
        }
        
    }
}