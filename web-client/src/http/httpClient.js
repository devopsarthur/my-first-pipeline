import axios from "axios";
import { config } from '../config';
import { STORAGE_KEYS } from '../constants';

export const httpClient
    = axios.create({ baseURL: config.backend.url });

httpClient.interceptors.request.use(async (config) => {
    // only set Authorization header if it is not a registration or login request
    if (isAuthRequest(config)) {
        console.log('is auth req');
        return config;
    }

    const authToken = window.localStorage.getItem(STORAGE_KEYS.AuthToken);
    if (authToken) {
        config.headers.Authorization = authToken;
    }
    
    return config;
});

const isAuthRequest = (config) => {
  const method = config.method;
    const endpoint = config.url;
    const isAuthEndpoint
        = endpoint === '/app/users/register' || endpoint === '/app/users/signin'
  return method === 'post' && isAuthEndpoint;
};
