import React, {createContext} from 'react';
import { STORAGE_KEYS } from '../constants';
import { validateAuthToken } from '../helper';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {

    const storeAuthToken = (newToken) => {
        window.localStorage.setItem(STORAGE_KEYS.AuthToken, newToken);
    }

    const getAuthToken = () => {
        return window.localStorage.getItem(STORAGE_KEYS.AuthToken);
    }

    const parseTokenPayload = () => {
        const tokenInStorage = getAuthToken();
        const jwtPayload = validateAuthToken(tokenInStorage);
        return jwtPayload;
    }

    return (<AuthContext.Provider value={
        { storeAuthToken, getAuthToken, parseTokenPayload }}>
        {children}
    </AuthContext.Provider>)
}