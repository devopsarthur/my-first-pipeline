import React from 'react';
import { Message } from 'semantic-ui-react'

const MyInfoComponent = ({ header, content, visible=false, success }) => {
    
    return (
        visible ? (
            success ?
                <Message
                    visible={visible}
                    positive
                    header={header}
                    content={content}
                />
                :
                <Message
                    visible={true}
                    error
                    header={header}
                    content={content}
                />
        ) :
            null
        )
        
}

export default MyInfoComponent;