import React from 'react';
import { Dropdown } from 'semantic-ui-react'


const UsersDropdown = ({ users, onSelection }) => {

    const options = users.map(user => {
        const option = {
            key: user.username,
            text: user.username,
            value: user.username
        }

        return option;
    })

    return (
        <div>
            <Dropdown
                onChange={onSelection}
                placeholder='Recipient'
                fluid multiple selection options={options} 
            />
        </div>
    )
}

export default UsersDropdown;








