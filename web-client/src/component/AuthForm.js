import React, { useState } from 'react';
import MyInputField from './MyInputField';

const AuthForm = ({
    isRegistration = false,
    onSubmitAuthForm,
    isSuccess,                        
    isError,
    errors }) => {
    
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const onSubmit = async (evt) => {
        evt.preventDefault();
        await onSubmitAuthForm(username, password);
    }
    

    const buttonClazz = isRegistration ? "ui orange button" : "ui green button";

    const registerLink = () => {
        return (
            <div>
                <br />
                Not yet a user? &nbsp; <a href="/register/" className="highlight">Register from here.</a>
                <br/>
            </div>
        )   
    }

    const loginLink = () => {
        return (
            <div>
                <br />
                Already have an account? &nbsp; <a href="/login/" className="highlight">Let's Login.</a>
                <br/>
            </div>
        )   
    }

    const showErrors = () => {
        
        return (
            <div className="ui negative message">
                <div className="header">Error message from backend:</div>
                <ul className="list">
                    {errors?.map((err, index) => (
                        <li key={new Date().getTime() + index}>
                            {err.message}
                            <br /><br />
                        </li>))}
                    <br />
                </ul>
            </div>
        )
    }

    const showSuccessMessage = () => {
        return (
            <div className="ui positive message">
                <div className="header">
                    Your user registration was successful.
                </div>
                <p>You may  
                    <a href="/login/" className="highlight">
                        <b> now Login</b>
                    </a> with the username you have chosen</p>
                </div>
                
        )
    }

    return (
        <form className="ui form" onSubmit={onSubmit}>
            <MyInputField
                label="Username:"
                name="username"
                placeholder="Your username"
                value={username}
                onChange={setUsername}
            />

            <MyInputField
                label="Password:"
                name="password"
                placeholder="Your password"
                isPassword={true}
                value={password}
                onChange={setPassword}
            />

            {isError ? showErrors() : null}<br />
            {isSuccess && isRegistration ? showSuccessMessage() : null}

            <button className={buttonClazz} type="submit">
                {isRegistration ? "Register" : "Login"}
            </button>

            {isRegistration ? loginLink() : registerLink()}
        </form>
    );
}

export default AuthForm;