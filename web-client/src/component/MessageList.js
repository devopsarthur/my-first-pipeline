import React from 'react';
import MessageItem from '../component/MessageItem';

const MessageList = ({ messages, activeSenders }) => {
    return (
        <div className="ui celled list">
            {messages.map(msg => {
                const senderActive = activeSenders?.includes(msg.sender);
                return <MessageItem key={msg.date} msg={msg} senderActive={senderActive}/>
            })}
            
        </div>
    );
}

export default MessageList