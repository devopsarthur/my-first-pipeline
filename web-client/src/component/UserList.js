import React from 'react';
import UserItem from '../component/UserItem';

const UserList = ({ users }) => {
    return (
        <div className="ui celled list">
            {users.map((user, index) => <UserItem key={new Date().getTime()+index} user={user}/>)}
            
        </div>
    );
}

export default UserList