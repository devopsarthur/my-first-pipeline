import React from 'react';

const UserItem = ({ user }) => {
    const { username, available } = user;
    const statusClazz = available ? "ui green label" : "ui grey label";
    return (
        <div className="item">
            <i className="user icon"></i>
            <div className="content">
                <div className="header">{username}</div>
                <div className={statusClazz}>{available ? "Available" : "Unavailable"}: {available}</div>
            </div>
        </div>
    )
}

export default UserItem;






