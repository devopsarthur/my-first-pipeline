import React from 'react';

const MyHeader = ({onTabClick, activeTab, username}) => {
    

    const getTabClazzName = (index) => {
        return activeTab === index ? "item active" : "item";
    }
    return (
        <div className="ui menu">
            <a href="#" className={getTabClazzName(1)} onClick={()=> onTabClick(1)}>
                <i className="mail icon" />
                Inbox
            </a>
            <a href="#" className={getTabClazzName(2)} onClick={()=> onTabClick(2)}>
                <i className="plus icon" />
                Send message
            </a>
            <a href="#" className={getTabClazzName(3)} onClick={()=> onTabClick(3)}>
                <i className="user icon" />
                Users
            </a>
  
            <div className="right menu">
                <a href="#" className="item">
                    Hello, {username}
                </a>
                <a href="/login/" className="item">
                    <i className="sign-out icon" />
                    Logout
                </a>
                
            </div>
        </div>
    )
}

export default MyHeader;