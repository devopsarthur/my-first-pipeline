import React from 'react';

const MessageItem = ({ msg, senderActive }) => {
    console.log(`senderActive: ${senderActive}`);
    const { message, sender, date } = msg;
    const senderLabelClazz = senderActive ? "ui green label" : "ui grey label";
    return (
        <div className="item">
            <i className="bullhorn icon"></i>
            <div className="content">
                <div className="header">{message}</div>
                <div className={senderLabelClazz}>Sender: {sender}</div>
                <div className="green">{date}</div>
            </div>
        </div>
    )
}

export default MessageItem;






