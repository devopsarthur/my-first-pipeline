import React from 'react';

const MyInputField = ({isPassword=false, label, name, placeholder, value, onChange,}) => {
    return (
        <div className="field">
            <div className="ui input focus">
                <label>{label}</label>
                <input
                    size='small'
                    style={styles.input}
                    type={isPassword ? "password" : "text"}
                    name={name}
                    value={value}
                    onChange={(event) => onChange(event.target.value)}
                    placeholder={placeholder}
                />
            </div>
        </div>
    )
}

const styles = {
    input: {
        width: 100,
    }
}

export default MyInputField;