export const config = {
    backend: {
        url: 'http://virta-assignment.io'
    },

    jwt: {
        secret: process.env.JWT_SECRET || 'abcd'
    },
}
