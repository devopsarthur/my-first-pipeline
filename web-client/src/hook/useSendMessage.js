import { useMutation } from 'react-query';
import { httpClient } from '../http/httpClient';


const sendMessage = async ({ recipient, message }) => {
    console.log(`send message : ${recipient}, ${message}`);
    const response
        = await httpClient.post(`/app/messages/users/${recipient}`, { message });
    return response.data;
}

const useSendMessage = (callbacks) => {
    return  useMutation(['send-message'],
            sendMessage,
        {
            onMutate: () => callbacks.onMutate(),
            onSuccess: () => callbacks.onSuccess(),   
            onError: (err) => callbacks.onError(err)
            });
    
}

export default useSendMessage;