
import {useQuery} from 'react-query';
import { httpClient } from '../http/httpClient';

export const usersQueryKey = 'all-users';

const useUsers = () => {
    return useQuery([usersQueryKey], () => httpClient.get(`/app/users`), {
      cacheTime: 30 * 1000, //10 sec
    });

};

export default useUsers;