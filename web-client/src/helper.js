import jwt from 'jsonwebtoken';
import { config } from './config';

export const validateAuthToken = (authToken) => {
    let payload;
    jwt.verify(authToken, config.jwt.secret, function (err, decoded) {
        if (err) {
            console.log(`decode error: ${err.message}`);
        } else {
            payload = decoded;
        }
    });
    return payload;
};
