import React, {useState, useContext} from 'react';
import AuthForm from '../component/AuthForm';
import { useMutation } from 'react-query';
import { httpClient } from '../http/httpClient';
import { AuthContext } from '../context/AuthContext';

const RegisterPage = () => {
    const [errors, setErrors] = useState();

    const { storeAuthToken } = useContext(AuthContext);

    const {mutateAsync: registerUserAsync,
        isSuccess,
        isError,
        status: registerUserStatus }
        = useMutation((values) => httpClient.post('/app/users/register', values));

    const onSubmitAuthForm = async (username, password) => {
        try {
            const response = await registerUserAsync({ username, password });
            storeAuthToken(response.data.authToken);
            console.log(`REPS-data: ${JSON.stringify(response.data)}`);
        } catch (err) {
            setErrors(err?.response.data?.errors);
            console.log(`${registerUserStatus} Failed to register user. ${JSON.stringify(err?.response.data)}`);
        }
            
    } 

    return (
        <div className="ui middle aligned center aligned grid">
            <div className="column">
                <h2 className="ui teal image header">
                    <div className="content">
                        Registration
                    </div>
                </h2>
                <div className="ui container">
                    <div className="ui segment">
                        <AuthForm
                            isRegistration={true}
                            onSubmitAuthForm={onSubmitAuthForm}
                            isSuccess={isSuccess}
                            isError={isError}
                            errors={errors} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default RegisterPage;