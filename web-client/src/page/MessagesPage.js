import React, {useContext} from 'react';
import { useQuery } from 'react-query';
import { AuthContext } from '../context/AuthContext';
import MessageList from '../component/MessageList';
import { httpClient } from '../http/httpClient';

const MessagesPage = ({users}) => {
    
    const { parseTokenPayload } = useContext(AuthContext);
    const { username } = parseTokenPayload();

    const activeSenders = users?.filter(user => user.available === true);

    const { data: response,
        isLoading,
        isFetching,
        isError,
        error } = useQuery(['message-list', username],
            () => httpClient.get(`/app/messages/users/${username}`));

    
    const displayMessageList = () => {
        const messages = response.data.messages;
        return (
            <>
                <MessageList messages={messages} activeSenders={activeSenders}/>
                <br />
                {isFetching ? <div>Updating...</div> : null}
            </>
        );
    }

    return (
        <div className="container">
            {isLoading ?
                <div>Loading data...</div>
                :
                isError ?
                    <div>{error.message}</div>
                    :
                    displayMessageList()
                }
        </div>
    )
}

export default MessagesPage;