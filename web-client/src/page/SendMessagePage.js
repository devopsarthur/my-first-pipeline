import React, {useState} from 'react';
import { Grid, Segment, Header, Form, TextArea, Button } from 'semantic-ui-react'
import UsersDropdown from '../component/UsersDropdown';
import { useMutation } from 'react-query';
import { httpClient } from '../http/httpClient';
import MyInfoComponent from '../component/MyInfoComponent';
import useSendMessage from '../hook/useSendMessage';

const SendMessagePage = ({ users }) => {

    const [recipients, setRecipients] = useState([]);
    const [message, setMessage] = useState(null);
    const [resultInfo, setResultInfo] = useState(null);

    
    const sendMessageCallbacks = {
        onMutate: () => {
            console.log('onMutate()');
            setResultInfo(null);
        },

        onSuccess: () => {
            console.log('send successfully');
            setResultInfo({
                header: 'Done.',
                content: 'Message sent successfully!',
                isSuccess: true
            })
        },

        onError: (err) => {
            console.log(err.message);
            setResultInfo({
                header: 'Error from backend:',
                content: err.message,
                isSuccess: false
            })
        }
            
    }

    const {mutateAsync: sendMessageAsync, isLoading: isSending}= useSendMessage(sendMessageCallbacks);
    
    const onRecipientsSelection = (e, {value}) => {
        setRecipients(value);
        setResultInfo(null);
    }

    const validateInputs = () => {
        if (recipients.length === 0) {
            setResultInfo({
                header: 'Validation error',
                content: 'Please select recipients.',
                isSuccess: false
            });
        } else if (message === null || message.legnth === 0) {
            setResultInfo({
                header: 'Validation error',
                content: 'Cannot send empty message.',
                isSuccess: false
            });
        }
    }
    
    const onSendMessage = async () => {
        validateInputs();

        if (resultInfo !== null && !resultInfo?.isSuccess) {
            return;
        }
        console.log('validation passed, start sending message...');
        recipients.forEach(async recipient => {
            await sendMessageAsync({ recipient, message });
        })
    }
    
    return (
        <div className="container">
            <Grid className="segment centered">
                <Segment style={{width: '60%'}}>
                    
                    <Header as='h5'>Recipient(s):</Header>

                    <UsersDropdown
                        users={users}
                        onSelection={onRecipientsSelection}
                    />
                    
                    <Form>
                    
                        <Form.Field
                            id='form-textarea-control-opinion'
                            control={TextArea}
                            label='Message'
                            placeholder='Please write message here.'
                            onChange={(e, { value }) => {
                                setResultInfo(null);
                                setMessage(value);
                            }}
                        />

                        <MyInfoComponent
                            header={resultInfo?.header}
                            content={resultInfo?.content}
                            visible={resultInfo != null}
                            success={resultInfo?.isSuccess} />

                        <Button
                            content={isSending ? 'Sending...' : 'Send'}
                            primary
                            onClick={onSendMessage}
                        />
                    </Form>
                </Segment>
            </Grid>
        </div>
    )
}

export default SendMessagePage;