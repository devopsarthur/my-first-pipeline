import React from 'react';
import { useQueryClient, QueryClient } from 'react-query';
import UserList from '../component/UserList';
import useUsers from '../hook/useUsers';

const UsersPage = () => {


    const { isLoading,
        isError,
        error,
        data: response } = useUsers();
 
    return (
        <div className="container">
            {isLoading ?
                <div>Loading data...</div>
                :
                isError ?
                    <div>{error.message}</div>
                    :
                    <UserList users={response.data.users} />}
        </div>
    )
}

export default UsersPage;