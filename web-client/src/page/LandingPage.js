import React, {useContext, useState, useEffect} from 'react';
import { AuthContext } from '../context/AuthContext';
import MyHeader from '../component/MyHeader';
import MessagesPage from './MessagesPage';
import UsersPage from './UsersPage';
import SendMessagePage from './SendMessagePage';
import useUsers from '../hook/useUsers';

const LandingPage = () => {
    const [activeHeaderTab, setActiveHeaderTab] = useState(1);

    const toggleHeaderTab = (index) => {
        setActiveHeaderTab(index);
    }
    const { parseTokenPayload } = useContext(AuthContext);
    
    const {username} = parseTokenPayload();

    const { isLoading,
        isError,
        error,
        data: response } = useUsers();

    const displayContentOfTab = (tabIndex) => {
        const users = response.data.users;
        
        let content; 
        switch (tabIndex) {
            case 1: content = <MessagesPage users={users}/>
                break;
            case 2: content = <SendMessagePage users={users}/>
                break;
            case 3: content = <UsersPage />
                break;
            default:
                content = <MessagesPage users={users}/>
                break;
        }

        return content;
        
    }
    
    return (
        <div className="">
            <MyHeader
                onTabClick={toggleHeaderTab}
                activeTab={activeHeaderTab}
                username={username} />
            
            <div className="container">
                {isLoading ?
                    <div>Loading data...</div>
                    :
                    isError ?
                        <div>{error.message}</div>
                        :
                        displayContentOfTab(activeHeaderTab)}
            </div>
        </div>

    )
}

export default LandingPage;