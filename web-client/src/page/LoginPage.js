import React, { useState, useContext } from 'react';
import {useHistory} from 'react-router-dom';
import AuthForm from '../component/AuthForm';
import { useMutation } from 'react-query';
import { httpClient } from '../http/httpClient';
import { AuthContext } from '../context/AuthContext';

const LoginPage = () => {
    const history = useHistory();
    const [errors, setErrors] = useState();
    const { storeAuthToken } = useContext(AuthContext);
    
    const { mutateAsync: loginUserAsync,
        isSuccess,
        isError,
        status: loginUserStatus }
        = useMutation((values) => httpClient.post('/app/users/signin', values));
    
    const goToLandingPage = () => {
        const path = {pathname: '/landing'};
        history.push(path);
    }

    const onSubmitAuthForm = async (username, password) => {
        try {
            const response = await loginUserAsync({ username, password });
            storeAuthToken(response.data.authToken);
            console.log(`REPS-data: ${JSON.stringify(response.data)}`);
            goToLandingPage();
        } catch (err) {
            setErrors(err?.response?.data?.errors);
            console.log(`${loginUserStatus} Failed to sign in user. ${JSON.stringify(err?.response?.data)}`);
        }
            
    } 
    return (
        <div className="ui middle aligned center aligned grid">
            <div className="column">
                <h2 className="ui teal image header">
                    <div className="content">
                        Welcome!
                    </div>
                </h2>
                <div className="ui container">
                    <div className="ui segment">
                        <AuthForm
                            onSubmitAuthForm={onSubmitAuthForm}
                            isSuccess={isSuccess}
                            isError={isError}
                            errors={errors}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginPage;