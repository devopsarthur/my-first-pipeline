
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import {AuthProvider} from './context/AuthContext';
import LoginPage from './page/LoginPage';
import RegisterPage from './page/RegisterPage';
import LandingPage from './page/LandingPage';

const Content = () => {
    return (
        <div>
            <Route exact path="/" component={LoginPage} />
            <Route path="/login/" component={LoginPage} />
            <Route path="/register/" component={RegisterPage} />
            <Route path="/landing/" component={LandingPage} />
        </div>
        )
}

const queryClient = new QueryClient();

const App = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <Router>
                <div>
                    <AuthProvider>
                        <Content />
                    </AuthProvider>
                </div>
            </Router>
            <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
    )
}

export default App;
